package addonisproject.addonis.addonsDataBase.services;

import addonisproject.addonis.Factory;
import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.addons.Status;
import addonisproject.addonis.addonsDataBase.models.addons.UserDetails;
import addonisproject.addonis.addonsDataBase.models.dtos.AddonCardDTO;
import addonisproject.addonis.addonsDataBase.repos.AddonsRepository;
import addonisproject.addonis.addonsDataBase.repos.FeaturedRepository;
import addonisproject.addonis.addonsDataBase.repos.StatusRepository;
import addonisproject.addonis.exceptions.DuplicateEntityException;
import addonisproject.addonis.exceptions.SizeExceededException;
import addonisproject.addonis.utils.Helper;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

import static addonisproject.addonis.Factory.*;
import static addonisproject.addonis.utils.Constants.PER_PAGE_SEARCH;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class AddonServiceImplTests {

    @Mock
    AddonsRepository addonsRepository;
    @Mock
    Helper helper;
    @Mock
    StatusRepository statusRepository;
    @Mock
    FeaturedRepository featuredRepository;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @InjectMocks
    AddonsServiceImpl addonsService;

    @Test
    public void getAllAddonsShould_CallRepository() {
        // Arrange, Act
        addonsService.getAll();
        //  Assert
        Mockito.verify(addonsRepository, times(1))
                .findAll();
    }

    @Test
    public void getAllStatusApprovedShould_ReturnOnlyApproved() {
        //Arrange
        Mockito.when(addonsRepository.findAll())
                .thenReturn(getAllAddons());
        //Act
        List<Addon> result = addonsService.getAllStatusApproved();
        //Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void getAllStatusPendingShould_ReturnOnlyApproved() {
        //Arrange
        Mockito.when(addonsRepository.findAll())
                .thenReturn(getAllAddons());
        //Act
        List<Addon> result = addonsService.getAllStatusPending();
        //Assert
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void getByIdShould_ReturnIfAddonsExists() {
        //Arrange
        Mockito.when(addonsRepository.findById(anyInt())).
                thenReturn(Optional.of(createApprovedAddon()));
        //Act
        Addon addon = addonsService.getById(1);
        //Assert
        Assert.assertEquals(1, addon.getId());
    }

    @Test
    public void getByIdShould_ThrowExceptionIfAddonsDoesntExist() {
        //Arrange
        Mockito.when(addonsRepository.
                findById(anyInt())).thenReturn(Optional.empty());
        //Act,Assert
        exception.expect(EntityNotFoundException.class);
        addonsService.getById(1);
    }

    @Test
    public void getByNameShould_ReturnIfAddonsExists() {
        //Arrange
        Mockito.when(addonsRepository.getByName(anyString()))
                .thenReturn(Optional.of(createApprovedAddon()));
        //Act
        Addon addon = addonsService.getByName("Test Addon");
        //Assert
        Assert.assertEquals("Test Addon", addon.getName());
    }

    @Test
    public void getByNameShould_ThrowExceptionIfAddonsDoesntExist() {
        //Arrange
        Mockito.when(addonsRepository.getByName(anyString()))
                .thenReturn(Optional.empty());
        //Act,Assert
        exception.expect(EntityNotFoundException.class);
        addonsService.getByName("Ivan");
    }

    @Test
    public void searchByNameShould_ReturnIfAddonsContainingNameExist() {
        //Arrange
        Mockito.when(addonsRepository.searchByName(anyString()))
                .thenReturn(getAllAddons());
        //Act
        List<Addon> addonList = addonsService.searchByName("Test");
        //Assert
        Assert.assertEquals(3, addonList.size());
    }

    @Test
    public void createAddonShould_CallRepository(){
        //Arrange
        Addon addon = createPendingAddon();
        Mockito.when(addonsRepository.saveAndFlush(addon))
                .thenReturn(addon);
        //Act
        addonsService.createAddon(addon);
        //Assert
        Mockito.verify(addonsRepository,times(1))
                .saveAndFlush(addon);
    }

    @Test
    public void createAddonShould_Throw_WhenNameExists(){
        //Arrange
        Addon addon = createPendingAddon();
        Mockito.when(addonsRepository.checkAddonNameExists(anyString()))
                .thenReturn(true);
        //Act, Assert
        exception.expect(DuplicateEntityException.class);
        addonsService.createAddon(addon);
    }

    @Test
    public void updateAddonShould_CallRepository_WhenInputIsCorrect() {
        //Arrange
        Addon addon = createApprovedAddon();
        Mockito.when(addonsRepository.save(addon))
                .thenReturn(addon);
        Mockito.when(addonsRepository.findById(anyInt()))
                .thenReturn(Optional.of(addon));
        //Act
        addonsService.updateAddon(addon);
        //Assert
        Mockito.verify(addonsRepository, times(1))
                .save(addon);
    }

    @Test
    public void updateAddonShould_ThrowException_WhenNewNameAlreadyExists() {
        //Arrange
        Addon addon = createApprovedAddon();
        Addon sameAddonWithDifferentName = createApprovedAddon();
        sameAddonWithDifferentName.setName("MustFail");
        Mockito.when(addonsRepository.findById(anyInt()))
                .thenReturn(Optional.of(sameAddonWithDifferentName));
        Mockito.when(addonsRepository.checkAddonNameExists(anyString()))
                .thenReturn(true);
        //Act,Assert
        exception.expect(DuplicateEntityException.class);
        addonsService.updateAddon(addon);
    }

    @Test
    public void deleteAddonShould_ChangeStatusToDeleted(){
        //Arrange
        Addon addon = createApprovedAddon();
        Status status = createDeletedStatus();
        Mockito.when(statusRepository.findById(anyInt()))
                .thenReturn(Optional.of(status));
        //Act
        addonsService.deleteAddon(addon);
        //Assert
        Mockito.verify(addonsRepository,times(1))
                .save(addon);
    }

    @Test
    public void approveAddonShould_ChangeStatusToApproved(){
        //Arrange
        Addon addon = createPendingAddon();
        Status status = createApprovedStatus();
        Mockito.when(statusRepository.findById(anyInt()))
                .thenReturn(Optional.of(status));
        //Act
        addonsService.approveAddon(addon);
        //Assert
        Mockito.verify(addonsRepository,times(1))
                .save(addon);
    }

    @Test
    public void updateNumberDownloads_Should_CallRepository(){
        //Arrange
        Addon addon = createApprovedAddon();
        //Act
        addonsService.updateNumberDownloadsOnClick(addon.getId());
        //Assert
        Mockito.verify(addonsRepository, times(1))
                .updateNumberDownloadsOnClick(anyInt());
    }

    @Test
    public void getFeaturedShould_ReturnWhenExists(){
        //Arrange
        Mockito.when(featuredRepository.getFeatured())
                .thenReturn(Arrays.asList(createApprovedAddon()));
        //Act
        List<Addon> featured = addonsService.getFeaturedAddons();
        //Assert
        Assert.assertEquals(1, featured.size());
    }

    @Test
    public void getFeaturedShould_ReturnZeroWhenDoesNotExist(){
        //Arrange
        Mockito.when(featuredRepository.getFeatured())
                .thenReturn(Collections.emptyList());
        //Act
        List<Addon> featured = addonsService.getFeaturedAddons();
        //Assert
        Assert.assertEquals(0, featured.size());
    }

    @Test
    public void addToFeaturedShould_CallRepository(){
        //Arrange
        Addon addon = createApprovedAddon();
        Addon secondAddon = createApprovedAddon();
        secondAddon.setName("Test");
        Mockito.when(featuredRepository.getFeatured())
                .thenReturn(Arrays.asList(secondAddon));
        //Act
        addonsService.addToFeatured(addon);
        //Assert
        Mockito.verify(featuredRepository,times(1))
                .getFeatured();
    }

    @Test
    public void addToFeaturedShould_Throw_WhenSizeExceeded(){
        //Arrange
        Addon addon = createApprovedAddon();
        Mockito.when(featuredRepository.getFeatured())
                .thenReturn(getTenFeaturedAddons());
        //Act, Assert
        exception.expect(SizeExceededException.class);
        addonsService.addToFeatured(addon);
    }

    @Test
    public void removeFromFeaturedShould_CallRepository(){
        //Arrange
        Addon addon = createApprovedAddon();
        //Act
        addonsService.removeFromFeatured(addon);
        //Assert
        Mockito.verify(featuredRepository,times(1))
                .getByAddonId(addon.getId());
    }

    @Test
    public void getPopularAddonsShould_ReturnTopFiveMostDownloadedAddons() {
        //Arrange
        Mockito.when(addonsRepository.findAll())
                .thenReturn(getPopularAddons());
        List<Integer> expectedResult = new ArrayList<>();
        expectedResult.add(16);
        expectedResult.add(9);
        expectedResult.add(6);
        expectedResult.add(4);
        expectedResult.add(3);
        //Act
        List<Integer> numberOfDownloadsPopularAddons = addonsService
                .getPopularAddons()
                .stream()
                .map(Addon::getNumberDownloads).collect(Collectors.toList());
        //Assert
        Assert.assertEquals(expectedResult, numberOfDownloadsPopularAddons);
    }

    @Test
    public void getNewestShould_ReturnWhenExists(){
        //Arrange
        Mockito.when(addonsRepository.findAll())
                .thenReturn(Arrays.asList(createApprovedAddon()));
        //Act
        List<Addon> newestAddons = addonsService.getNewestAddons();
        //Assert
        Assert.assertEquals(1, newestAddons.size());
    }

    @Test
    public void getNewestShould_ReturnZeroWhenDoesNotExist(){
        //Arrange
        Mockito.when(addonsRepository.findAll())
                .thenReturn(Collections.emptyList());
        //Act
        List<Addon> newestAddons = addonsService.getNewestAddons();
        //Assert
        Assert.assertEquals(0, newestAddons.size());
    }

    @Test
    public void getUserApprovedShould_ReturnWhenExists(){
        //Arrange
        UserDetails user = createUser();
        Mockito.when(addonsRepository.findAll())
                .thenReturn(Arrays.asList(createApprovedAddon(), createPendingAddon()));
        //Act
        List<Addon> userApproved = addonsService.getUserApprovedAddons(user.getId());
        //Assert
        Assert.assertEquals(1, userApproved.size());
    }

    @Test
    public void getUserApprovedShould_ReturnZeroWhenDoesNotExist(){
        //Arrange
        UserDetails user = createUser();
        Mockito.when(addonsRepository.findAll())
                .thenReturn(Arrays.asList(createPendingAddon()));
        //Act
        List<Addon> userApproved = addonsService.getUserApprovedAddons(user.getId());
        //Assert
        Assert.assertEquals(0, userApproved.size());
    }

    @Test
    public void getUserPendingShould_ReturnWhenExists(){
        //Arrange
        UserDetails user = createUser();
        Mockito.when(addonsRepository.findAll())
                .thenReturn(Arrays.asList(createApprovedAddon(), createPendingAddon()));
        //Act
        List<Addon> userPending = addonsService.getUserPendingAddons(user.getId());
        //Assert
        Assert.assertEquals(1, userPending.size());
    }

    @Test
    public void getUserPendingShould_ReturnZeroWhenDoesNotExist(){
        //Arrange
        UserDetails user = createUser();
        Mockito.when(addonsRepository.findAll())
                .thenReturn(Arrays.asList(createApprovedAddon()));
        //Act
        List<Addon> userPending = addonsService.getUserPendingAddons(user.getId());
        //Assert
        Assert.assertEquals(0, userPending.size());
    }

    @Test
    public void searchAddonsShould_ReturnEmptyList_WhenNoAddonMatchesCriteria() {
        //Arrange,Act
        List<Addon> resultList = addonsService.searchAddons("IDE5", null, null);
        //Assert
        Assert.assertEquals(0, resultList.size());
    }

    @Test
    public void searchAddonsShould_ReturnList_WhenAddonsIDEMatchCriteria() {
        //Arrange
        List<Addon> addonList = getFilterableApprovedAddons();
        Mockito.when(addonsRepository.findAll()).thenReturn(addonList);
        //Act
        List<Addon> resultList = addonsService.searchAddons("IDE3", null, null);
        //Assert
        Assert.assertEquals(1, resultList.size());
    }

    @Test
    public void searchAddonsShould_ReturnSortedByNameList_WhenSortCategoryIsName() {
        //Arrange
        List<Addon> addonList = getFilterableApprovedAddons();
        List<Addon> expected = getSortedByNameAddons();
        Mockito.when(addonsRepository.findAll()).thenReturn(addonList);
        Mockito.when(helper.sortByName(anyList())).thenReturn(expected);
        //Act
        List<Addon> resultList = addonsService.searchAddons(null, null, "name");
        List<String> resultNames = resultList.stream().map(Addon::getName).collect(Collectors.toList());
        List<String> expectedNames = expected.stream().map(Addon::getName).collect(Collectors.toList());
        //Assert
        Assert.assertEquals(expectedNames, resultNames);
    }

    @Test
    public void searchAddonsShould_ReturnSortedByDownloadsList_WhenSortCategoryIsDownloads() {
        //Arrange
        List<Addon> addonList = Factory.getFilterableApprovedAddons();
        List<Addon> expected = getSortedByDownloads();
        Mockito.when(addonsRepository.findAll()).thenReturn(addonList);
        Mockito.when(helper.sortByName(anyList())).thenReturn(expected);
        //Act
        List<Addon> resultList = addonsService.searchAddons(null, null, "name");
        List<Integer> resultNames = resultList
                .stream()
                .map(Addon::getNumberDownloads)
                .collect(Collectors.toList());

        List<Integer> expectedNames = expected
                .stream()
                .map(Addon::getNumberDownloads)
                .collect(Collectors.toList());
        //Assert
        Assert.assertEquals(expectedNames, resultNames);
    }

    @Test
    public void paginateShould_ReturnMaxAddonsPerPage_WhenMoreAddonsAreSelected()
    {
        //Arrange
        List<AddonCardDTO>addonList=new ArrayList<>(getFiveAddonCards());
        addonList.addAll(getFiveAddonCards());
        addonList.addAll(getFiveAddonCards());

        //Act
        List<AddonCardDTO>result=addonsService.paginate(addonList,1);

        //Assert
        Assert.assertEquals(PER_PAGE_SEARCH,result.size());
    }

    @Test
    public void paginateShould_ReturnCorrectNumberOfAddons_WhenLessThanPerPageLimitAddonsAreSelected()
    {
        //Arrange
        List<AddonCardDTO>addonList=new ArrayList<>(getFiveAddonCards());
        addonList.addAll(getFiveAddonCards());
        addonList.addAll(getFiveAddonCards());

        //Act
        List<AddonCardDTO>result=addonsService.paginate(addonList,2);

        //Assert
        Assert.assertEquals((addonList.size()-PER_PAGE_SEARCH),result.size());
    }

    @Test
    public void paginateShould_ReturnEmptyList_WhenLessAddonsAreSelected()
    {
        //Arrange
        List<AddonCardDTO>addonList=new ArrayList<>(getFiveAddonCards());
        addonList.addAll(getFiveAddonCards());
        addonList.addAll(getFiveAddonCards());

        //Act
        List<AddonCardDTO>result=addonsService.paginate(addonList,3);

        //Assert
        Assert.assertEquals(0,result.size());
    }
}
