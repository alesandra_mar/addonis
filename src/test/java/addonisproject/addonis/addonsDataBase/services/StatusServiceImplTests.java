package addonisproject.addonis.addonsDataBase.services;

import addonisproject.addonis.Factory;
import addonisproject.addonis.addonsDataBase.models.addons.Status;
import addonisproject.addonis.addonsDataBase.repos.StatusRepository;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class StatusServiceImplTests {

    @Mock
    StatusRepository statusRepository;

    @InjectMocks
    StatusServiceImpl statusService;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void getStatusByIdShould_ReturnStatus_WhenStatusExists(){
        //        Arrange
        Status expected = Factory.createApprovedStatus();
        Mockito.when(statusRepository.findById(anyInt())).thenReturn(Optional.of(expected));
        //        Act
        Status returned = statusService.getStatusById(anyInt());
        //        Assert
        Assert.assertSame(expected,returned);
    }

    @Test
    public void getStatusByIdShould_Throw_WhenStatusDoesNotExists(){
        //        Arrange, Act, Assert
        exception.expect(EntityNotFoundException.class);
        statusService.getStatusById(4);
    }
}
