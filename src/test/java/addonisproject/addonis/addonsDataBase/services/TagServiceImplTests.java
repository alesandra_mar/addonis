package addonisproject.addonis.addonsDataBase.services;

import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.addons.Tag;
import addonisproject.addonis.addonsDataBase.models.dtos.TagDTO;
import addonisproject.addonis.addonsDataBase.repos.AddonsRepository;
import addonisproject.addonis.addonsDataBase.repos.TagsRepository;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static addonisproject.addonis.Factory.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTests {

    @Mock
    TagsRepository tagsRepository;

    @Mock
    AddonsRepository addonsRepository;

    @InjectMocks
    TagsServiceImpl tagsService;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void getTagByNameShould_ReturnTag_WhenTagExists() {
        //        Arrange
        Tag expected = createTag();
        Mockito.when(tagsRepository.getByName(anyString()))
                .thenReturn(Optional.of(expected));
        //        Act
        Tag returned = tagsService.getTagByName(anyString());
        //        Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void getTagByNameShould_Throw_WhenTagNotExists() {
        //Arrange, Act, Assert
        exception.expect(EntityNotFoundException.class);
        tagsService.getTagByName(anyString());
    }

    @Test
    public void getTagByNameShould_CallRepository() {
        //Arrange
        Tag expected = createTag();
        Mockito.when(tagsRepository.getByName(anyString()))
                .thenReturn(Optional.of(expected));
        //Act
        tagsService.getTagByName(anyString());
        //Assert
        Mockito.verify(tagsRepository,
                times(1)).getByName(anyString());
    }

    @Test
    public void addTagToAddonShould_CallAddonRepository_WhenTagExists() {
        //        Arrange
        TagDTO tagDTO = createTagDto();
        Addon addon = createApprovedAddon();
        Tag tag = createTag();
        Mockito.when(tagsRepository.checkTagNameExists(anyString()))
                .thenReturn(true);
        Mockito.when(tagsRepository.getByName(anyString()))
                .thenReturn(Optional.of(tag));
        addon.getTags().add(tag);
        //        Act
        tagsService.addTagToAddon(addon, tagDTO);
        //        Assert
        Mockito.verify(addonsRepository, times(1))
                .save(addon);
    }

    @Test
    public void addTagToAddonShould_CallAddonRepository_WhenTagDoesNotExist() {
        //        Arrange
        TagDTO tagDTO = createTagDto();
        Addon addon = createApprovedAddon();
        Tag tag = createTag();
        Mockito.when(tagsRepository.checkTagNameExists(anyString()))
                .thenReturn(false);
        addon.getTags().add(tag);
        //        Act
        tagsService.addTagToAddon(addon, tagDTO);
        //        Assert
        Mockito.verify(addonsRepository, times(1))
                .save(addon);
    }

    @Test
    public void removeTagFromAddonShould_CallAddonRepository_WhenTagExists() {
        //        Arrange
        TagDTO tagDTO = createTagDto();
        Addon addon = createApprovedAddon();
        Tag tag = createTag();
        Mockito.when(tagsRepository.checkTagNameExists(anyString()))
                .thenReturn(true);
        Mockito.when(tagsRepository.getByName(anyString()))
                .thenReturn(Optional.of(tag));
        addon.getTags().add(tag);
        //        Act
        tagsService.removeTagFromAddon(addon, tagDTO);
        //        Assert
        Mockito.verify(addonsRepository, times(1))
                .save(addon);
    }

    @Test
    public void removeTagFromAddonShould_Throw_WhenTagDoesNotExistsInDatabase() {
        //        Arrange
        TagDTO tagDTO = createTagDto();
        Addon addon = createApprovedAddon();
        Tag tag = createTag();
        Mockito.when(tagsRepository.checkTagNameExists(anyString()))
                .thenReturn(false);
        //        Act, Assert
        exception.expect(EntityNotFoundException.class);
        tagsService.removeTagFromAddon(addon, tagDTO);
    }

    @Test
    public void removeTagFromAddonShould_Throw_WhenTagDoesNotExistsInAddonSet() {
        //        Arrange
        TagDTO tagDTO = createTagDto();
        Addon addon = createApprovedAddon();
        Tag tag = createTag();
        Mockito.when(tagsRepository.checkTagNameExists(anyString()))
                .thenReturn(true);
        Mockito.when(tagsRepository.getByName(anyString()))
                .thenReturn(Optional.of(tag));
        //        Act, Assert
        exception.expect(EntityNotFoundException.class);
        tagsService.removeTagFromAddon(addon, tagDTO);
    }
}
