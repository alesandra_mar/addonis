package addonisproject.addonis.addonsDataBase.services;

import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.addons.Rating;
import addonisproject.addonis.addonsDataBase.models.addons.UserDetails;
import addonisproject.addonis.addonsDataBase.repos.RatingRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static addonisproject.addonis.Factory.*;

@RunWith(MockitoJUnitRunner.class)
public class RatingServiceImplTests {

    @Mock
    RatingRepository ratingRepository;

    @InjectMocks
    RatingServiceImpl ratingService;

    @Test
    public void rateAddonShould_UpdateRating_WhenRatingExists(){
        //        Arrange
        Addon addon = createApprovedAddon();
        UserDetails user = createUser();
        Rating rating = createRating();
        Mockito.when(ratingRepository.checkRatingExists(addon.getId(), user.getId()))
                .thenReturn(true);
        //        Act
        ratingService.rateAddon(rating);
        //        Assert
        Mockito.verify(ratingRepository,Mockito.times(1))
                .updateRating(addon.getId(), user.getId(), rating.getRating());
    }

    @Test
    public void rateAddonShould_SaveRating_WhenRatingDoesNotExist(){
        //        Arrange
        Addon addon = createApprovedAddon();
        UserDetails user = createUser();
        Rating rating = createRating();
        Mockito.when(ratingRepository.checkRatingExists(addon.getId(), user.getId()))
                .thenReturn(false);
        //        Act
        ratingService.rateAddon(rating);
        //        Assert
        Mockito.verify(ratingRepository,Mockito.times(1))
                .save(rating);
    }

    @Test
    public void getAvgRatingShould_CallRepository(){
        //        Arrange
        Addon addon = createApprovedAddon();
        //        Act
        ratingService.getAvgRating(addon);
        //        Assert
        Mockito.verify(ratingRepository, Mockito.times(1))
                .getAddonRatings(addon.getId());
    }

    @Test
    public void getAvgRatingShould_ReturnAvgRating(){
        //        Arrange
        Addon addon = createApprovedAddon();
        Rating rating = createRating();
        List<Rating> ratings = new ArrayList<>();
        ratings.add(rating);
        Mockito.when(ratingRepository.getAddonRatings(addon.getId())).thenReturn(ratings);
        //        Act
        double avgRating = ratingService.getAvgRating(addon);
        //        Assert
        Assert.assertSame(rating.getRating(), (int)avgRating);
    }
}
