package addonisproject.addonis.controllers;

import addonisproject.addonis.exceptions.UserRegistrationException;
import addonisproject.addonis.addonsDataBase.models.Mapper;
import addonisproject.addonis.addonsDataBase.models.addons.UserDetails;
import addonisproject.addonis.addonsDataBase.models.dtos.UserDTO;
import addonisproject.addonis.addonsDataBase.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.io.IOException;
import java.util.logging.*;

import static addonisproject.addonis.utils.Constants.LOG_FOLDER;


/**
 * The class handles registration of new users
 */
@Controller
@RequestMapping("/register")
public class RegistrationController {

    private static Logger logger;
    private static FileHandler fh;
    private UsersService usersService;
    private Mapper mapper;


    @Autowired
    public RegistrationController(UsersService usersService,
                                  Mapper mapper) throws IOException {
        this.usersService = usersService;
        this.mapper = mapper;
        logger = Logger.getLogger(RegistrationController.class.getSimpleName());
        fh = new FileHandler(LOG_FOLDER+"RegistrationLog.log");
        logger.addHandler(fh);
    }

    @GetMapping()
    public String showRegistrationForm(Model model) {
        model.addAttribute("userDTO", new UserDTO());
        return "register";
    }

    @PostMapping()
    public String registerUser(@Valid @ModelAttribute UserDTO userDTO, BindingResult errors, Model model) {
        if (errors.hasErrors()) {
            return "register";
        }
        try {
            UserDetails userDetails = new UserDetails();
            userDetails = mapper.dtoToUser(userDTO, userDetails);
            usersService.createUser(userDetails, userDTO.getPassword(), userDTO.getRepeatPassword());
        } catch (UserRegistrationException e) {
            logger.log(Level.WARNING, e.getMessage(), e.getStackTrace());
            model.addAttribute("error", e.getMessage());
            return "register";
        }
        return "register-confirm";
    }
}
