package addonisproject.addonis.controllers;

import addonisproject.addonis.addonsDataBase.models.Mapper;
import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.addons.IDE;
import addonisproject.addonis.addonsDataBase.models.addons.Rating;
import addonisproject.addonis.addonsDataBase.models.addons.UserDetails;
import addonisproject.addonis.addonsDataBase.models.dtos.AddonCardDTO;
import addonisproject.addonis.addonsDataBase.models.dtos.AddonDTO;
import addonisproject.addonis.addonsDataBase.models.dtos.RatingDTO;
import addonisproject.addonis.addonsDataBase.models.dtos.TagDTO;
import addonisproject.addonis.addonsDataBase.services.contracts.*;
import addonisproject.addonis.binaryContentDataBase.models.BinaryContent;
import addonisproject.addonis.binaryContentDataBase.services.contracts.BinaryContentService;
import addonisproject.addonis.exceptions.*;
import com.github.rjeschke.txtmark.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static addonisproject.addonis.utils.Constants.*;
import static addonisproject.addonis.utils.ValidatorHelper.isAdmin;
import static addonisproject.addonis.utils.ValidatorHelper.isCreator;

/**
 * The Controller provides the basic CRUD operations to the end users and manages end-points related to
 * the addons.
 */
@Controller
@RequestMapping("/addons")
public class AddonsController {

    private AddonsService addonsService;
    private UsersService usersService;
    private StatusService statusService;
    private IDEService ideService;
    private RatingService ratingService;
    private TagsService tagsService;
    private BinaryContentService binaryContentService;
    private Mapper mapper;
    private APIService apiService;
    private static Logger logger;
    private static FileHandler fh;

    @Autowired
    public AddonsController(AddonsService addonsService,
                            UsersService usersService,
                            StatusService statusService,
                            IDEService ideService,
                            RatingService ratingService,
                            TagsService tagsService,
                            BinaryContentService binaryContentService,
                            Mapper mapper,
                            APIService apiService) throws IOException {
        this.addonsService = addonsService;
        this.usersService = usersService;
        this.statusService = statusService;
        this.ideService = ideService;
        this.ratingService = ratingService;
        this.tagsService = tagsService;
        this.binaryContentService = binaryContentService;
        this.mapper = mapper;
        this.apiService = apiService;
        logger = Logger.getLogger(AddonsController.class.getSimpleName());
        fh = new FileHandler(LOG_FOLDER + "Adddons.log");
        logger.addHandler(fh);
    }

    @ModelAttribute(name = "ides")
    public List<IDE> getAllIdes() {
        return ideService.getAll();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/pending")
    public String allPendingAddons(@RequestParam(name = "page", defaultValue = "1") Integer page,
                                   Model model) {
        List<AddonCardDTO> resultList = mapper.addonListToAddonCardList(addonsService.getAllStatusPending());
        model.addAttribute("pendingAddons", addonsService.paginate(resultList, page));
        model.addAttribute("pages", page);
        model.addAttribute("shouldShowNext", resultList.size() > page * PER_PAGE_SEARCH);
        model.addAttribute("shouldShowPrevious", page > 1);
        return "pending-addons";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/new")
    public String createAddonPage(Model model) {
        model.addAttribute("addon", new AddonDTO());
        return "create-new-addon";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/new")
    public String createAddon(@Valid @ModelAttribute("addon") AddonDTO addonDTO, BindingResult errors, Principal principal, Model model) {
        if (errors.hasErrors()) {
            return "create-new-addon";
        }
        if (addonDTO.getFile().isEmpty()) {
            model.addAttribute("message", "Please select a file to upload");
            return "create-new-addon";
        }
        try {
            Addon addonToCreate = new Addon();
            UserDetails creator = usersService.getUserByName(principal.getName());
            addonToCreate.setCreator(creator);
            mapper.dtoToAddon(addonDTO, addonToCreate);
            addonToCreate.setUploadDate(new Date());
            addonToCreate.setStatus(statusService.getStatusById(1));
            addonsService.createAddon(addonToCreate);
            return "redirect:" + "/users/" + creator.getId() + "/pending";
        } catch (DuplicateEntityException | FileProblemException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
            model.addAttribute("errors", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/{id}")
    public String addonPage(@PathVariable int id, Model model, Principal principal) {
        if (principal == null) {
            if (addonsService.getById(id).getStatus().getId() == 1) {
                return "redirect:/login";
            }
        }
        if (addonsService.getById(id).getStatus().getId() == 1
                && (!isCreator(addonsService.getById(id), principal) && !isAdmin())) {
            return "access-denied";
        }
        try {
            model.addAttribute("addon", addonsService.getById(id));
            model.addAttribute("averageRating", ratingService.getAvgRating(addonsService.getById(id)));
            List<AddonCardDTO> cardDTOSList = mapper.addonListToAddonCardList(addonsService.getFeaturedAddons());
            List<String> featuredNameList = cardDTOSList
                    .stream()
                    .map(AddonCardDTO::getName)
                    .collect(Collectors.toList());
            model.addAttribute("featuredList", featuredNameList);
            model.addAttribute("ratingDto", new RatingDTO());
            model.addAttribute("tagDto", new TagDTO());
            model.addAttribute("openIssues", apiService.getCountIssues(addonsService.getById(id)));
            model.addAttribute("pullRequests", apiService.getCountPulls(addonsService.getById(id)));
            model.addAttribute("lastCommitMessage", apiService.getLastCommit(addonsService.getById(id)).getMessage());
            model.addAttribute("lastCommitDate", mapper.simplifyDateFormat(apiService.getLastCommit(addonsService.getById(id)).getLastCommitDate()));
            return "addon-page";
        } catch (EntityNotFoundException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
            model.addAttribute("errors", e.getMessage());
            return "error";
        } catch (ApiConnectionException e) {
            logger.log(Level.WARNING, e.getMessage(), e.getStackTrace());
            model.addAttribute("openIssues", CONNECTION_ERROR_MESSAGE);
            model.addAttribute("pullRequests", CONNECTION_ERROR_MESSAGE);
            model.addAttribute("lastCommitMessage", CONNECTION_ERROR_MESSAGE);
            model.addAttribute("lastCommitDate", CONNECTION_ERROR_MESSAGE);
            return "addon-page";
        }
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/{id}/update")
    public String updateAddon(@PathVariable int id, Principal principal, Model model) {
        try {
            Addon addon = addonsService.getById(id);
            AddonDTO addonDTO = mapper.addonToDTO(new AddonDTO(), addon);
            if (!isCreator(addon, principal) && !isAdmin()) {
                throw new InvalidOperationException(USER_NOT_UNAUTHORIZED_TO_UPDATE_ADDON);
            }
            model.addAttribute("AddonDTO", addonDTO);
            return "update";
        } catch (EntityNotFoundException | InvalidOperationException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
            model.addAttribute("errors", e.getMessage());
            return "error";
        }
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/{id}/update")
    public String updateAddon(@PathVariable int id,
                              @Valid @ModelAttribute("AddonDTO") AddonDTO addonDTO,
                              BindingResult error, Model model, Principal principal) {
        String redirect = "redirect:/addons/";
        if (error.hasErrors()) {
            return "update";
        }
        try {
            Addon addon = addonsService.getById(id);
            if (!isCreator(addon, principal) && !isAdmin()) {
                throw new InvalidOperationException(USER_NOT_UNAUTHORIZED_TO_UPDATE_ADDON);
            }
            addon = mapper.dtoToAddon(addonDTO, addon);
            addonsService.updateAddon(addon);
            redirect += String.valueOf(id);
            return redirect;
        } catch (EntityNotFoundException |
                FileProblemException |
                InvalidOperationException |
                DuplicateEntityException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
            model.addAttribute("errors", e.getMessage());
            return "error";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/{id}/approve")
    public String approveAddon(@PathVariable int id, Model model, Principal principal) {
        try {
            Addon addon = addonsService.getById(id);
            addonsService.approveAddon(addon);
            return "redirect:/addons/pending";
        } catch (EntityNotFoundException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
            model.addAttribute("errors", e.getMessage());
            return "error";
        }
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/{id}/delete")
    public String deleteAddon(@PathVariable int id, Principal principal, Model model) {
        try {
            Addon addon = addonsService.getById(id);
            if (!isCreator(addon, principal) && !isAdmin()) {
                throw new InvalidOperationException(USER_NOT_UNAUTHORIZED_TO_UPDATE_ADDON);
            }
            addonsService.deleteAddon(addon);
        } catch (EntityNotFoundException | InvalidOperationException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
            model.addAttribute("errors", e.getMessage());
            return "error";
        }
        return "redirect:/search";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/{id}/rate")
    public String rateAddon(@PathVariable int id, @ModelAttribute("ratingDto") RatingDTO ratingDTO, Principal principal, Model model) {
        try {
            Rating rating = new Rating();
            rating.setRating(ratingDTO.getRating());
            rating.setUserDetails(usersService.getUserByName(principal.getName()));
            rating.setAddon(addonsService.getById(id));
            ratingService.rateAddon(rating);
            return "redirect:/addons/{id}";
        } catch (EntityNotFoundException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
            model.addAttribute("errors", e.getMessage());
            return "error";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/{id}/featured")
    public String addToFeatured(@PathVariable int id, Model model) {
        try {
            Addon addon = addonsService.getById(id);
            addonsService.addToFeatured(addon);
            return "redirect:/addons/{id}";
        } catch (EntityNotFoundException | SizeExceededException | DuplicateEntityException e) {
            logger.log(Level.WARNING, e.getMessage(), e.getStackTrace());
            model.addAttribute("errors", e.getMessage());
            return "error";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/{id}/featured/remove")
    public String removeFromFeatured(@PathVariable int id, Model model, Principal principal) {
        try {
            Addon addon = addonsService.getById(id);
            addonsService.removeFromFeatured(addon);
            return "redirect:/addons/{id}";
        } catch (EntityNotFoundException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
            model.addAttribute("errors", e.getMessage());
            return "error";
        }
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/{id}/tag")
    public String addTag(@PathVariable int id, @ModelAttribute("tagDto") TagDTO tagDtO, Principal principal, Model model) {
        try {
            Addon addon = addonsService.getById(id);
            UserDetails user = usersService.getUserByName(principal.getName());
            if (!isCreator(addon, user) && !isAdmin()) {
                throw new InvalidOperationException(USER_NOT_UNAUTHORIZED_TO_ADD_TAGS);
            }
            tagsService.addTagToAddon(addon, tagDtO);
            return "redirect:/addons/{id}";
        } catch (EntityNotFoundException | InvalidOperationException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
            model.addAttribute("errors", e.getMessage());
            return "error";
        }
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/{id}/tag/delete")
    public String removeTag(@PathVariable int id, @ModelAttribute("tagDto") TagDTO tagDtO, Principal principal, Model model) {
        try {
            Addon addon = addonsService.getById(id);
            UserDetails user = usersService.getUserByName(principal.getName());
            if (!isCreator(addon, user) && !isAdmin()) {
                throw new InvalidOperationException(USER_NOT_UNAUTHORIZED_TO_ADD_TAGS);
            }
            tagsService.removeTagFromAddon(addon, tagDtO);
            return "redirect:/addons/{id}";
        } catch (EntityNotFoundException | InvalidOperationException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
            model.addAttribute("errors", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/{id}/downloadFile")
    public ResponseEntity<Resource> downloadFile(@PathVariable int id) {
        Addon addon = addonsService.getById(id);
        addonsService.updateNumberDownloadsOnClick(id);
        BinaryContent binaryContent = binaryContentService.getById(addon.getBinaryContent());
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(binaryContent.getType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + binaryContent.getName() + "\"")
                .body(new ByteArrayResource(binaryContent.getData()));
    }

    @GetMapping("/{id}/readme")
    public String getReadme(@PathVariable int id, Model model) {
        try {
            Addon addon = addonsService.getById(id);
            String result = Processor.process(apiService.getReadMe(addon));
            model.addAttribute("readme", result);
            return "read-me";
        } catch (ApiConnectionException e) {
            String errorMessage = String.format(ENTITY_NOT_FOUND_ERROR_MESSAGE, RESOURCE_TYPE);
            logger.log(Level.SEVERE, errorMessage, e.getStackTrace());
            model.addAttribute("errors", errorMessage);
            return "error";
        } catch (EntityNotFoundException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
            model.addAttribute("errors", e.getMessage());
            return "error";
        }
    }
}
