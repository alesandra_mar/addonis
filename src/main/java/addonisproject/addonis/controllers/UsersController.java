package addonisproject.addonis.controllers;


import addonisproject.addonis.addonsDataBase.models.Mapper;
import addonisproject.addonis.addonsDataBase.models.dtos.AddonCardDTO;
import addonisproject.addonis.exceptions.InvalidOperationException;
import addonisproject.addonis.addonsDataBase.models.addons.UserDetails;
import addonisproject.addonis.addonsDataBase.services.contracts.AddonsService;
import addonisproject.addonis.addonsDataBase.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.List;
import java.security.Principal;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static addonisproject.addonis.utils.Constants.*;
import static addonisproject.addonis.utils.ValidatorHelper.isAdmin;

/**
 * The class controls the date a user can see in his profile - addons he has created and their status
 */
@Controller
@RequestMapping("/users")
public class UsersController {

    private UserDetailsManager userDetailsManager;
    private UsersService usersService;
    private UserDetailsService userDetailsService;
    private AddonsService addonsService;
    private Logger logger;
    private FileHandler fh;
    private Mapper mapper;

    public UsersController(UserDetailsManager userDetailsManager,
                           UsersService usersService,
                           @Qualifier("userDetailsManager") UserDetailsService userDetailsService,
                           AddonsService addonsService, Mapper mapper) throws IOException {
        this.userDetailsManager = userDetailsManager;
        this.usersService = usersService;
        this.userDetailsService = userDetailsService;
        this.addonsService = addonsService;
        this.mapper = mapper;
        logger = Logger.getLogger(SearchController.class.getSimpleName());
        fh = new FileHandler(LOG_FOLDER + "Users.log");
        logger.addHandler(fh);
    }

    @GetMapping("/{id}")
    public String userProfile(@PathVariable int id,
                              @RequestParam (name = "page", defaultValue = "1") Integer page,
                              Model model) {
        model.addAttribute("user", usersService.getUserById(id));
        List<AddonCardDTO> resultList=mapper.addonListToAddonCardList(
                addonsService.getUserApprovedAddons(id));
        model.addAttribute("userAddons",addonsService.paginate(resultList,page) );
        model.addAttribute("userPending", addonsService.getUserPendingAddons(id));
        model.addAttribute("pages", page);
        model.addAttribute("shouldShowNext",resultList.size()>page*PER_PAGE_SEARCH);
        model.addAttribute("shouldShowPrevious",page>1);
        return "my-profile";
    }

    @GetMapping("/name/{username}")
    public String showUserByUsername(@PathVariable("username") String username) {
        UserDetails userDetails = usersService.getUserByName(username);
        int id = userDetails.getId();
        String url = "/users/" + id;
        return "redirect:" + url;
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/{id}/pending")
    public String getUserPendingAddons(@PathVariable int id,
                                       @RequestParam (name = "page", defaultValue = "1") Integer page,
                                       Model model, Principal principal) {
        try {
            UserDetails user = usersService.getUserById(id);
            if (!isAdmin() && !principal.getName().equals(user.getUsername())) {
                return "access-denied";
            }
            List<AddonCardDTO> resultList=mapper.addonListToAddonCardList(
                    addonsService.getUserPendingAddons(id));
            model.addAttribute("pendingAddons",addonsService.paginate(resultList,page) );
            model.addAttribute("pages", page);
            model.addAttribute("shouldShowNext",resultList.size()>page*PER_PAGE_SEARCH);
            model.addAttribute("shouldShowPrevious",page>1);
            return "pending-addons";
        } catch (EntityNotFoundException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
            model.addAttribute("errors", e.getMessage());
            return "error";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id) {
        UserDetails userDetails = usersService.getUserById(id);
        org.springframework.security.core.userdetails.UserDetails springUserDetails;
        springUserDetails = userDetailsService.loadUserByUsername(userDetails.getUsername());
        springUserDetails = new User(
                springUserDetails.getUsername(),
                springUserDetails.getPassword(),
                false,
                springUserDetails.isAccountNonExpired(),
                springUserDetails.isCredentialsNonExpired(),
                springUserDetails.isAccountNonLocked(),
                springUserDetails.getAuthorities());
        userDetailsManager.updateUser(springUserDetails);
        usersService.deleteUser(userDetails);
        String url = "/users";
        return "redirect:" + url;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping
    public String getAllUsers(Model model) {
        try {
            if (!isAdmin()) {
                throw new InvalidOperationException(ADMIN_AUTHORIZATION_REQUIRED);
            }
            model.addAttribute("allUsers", usersService.getAllNonAdmins());
            return "users";
        } catch (InvalidOperationException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
            model.addAttribute("errors", e.getMessage());
            return "error";
        }
    }
}
