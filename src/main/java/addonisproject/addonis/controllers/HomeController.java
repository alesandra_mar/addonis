package addonisproject.addonis.controllers;


import addonisproject.addonis.addonsDataBase.models.Mapper;
import addonisproject.addonis.addonsDataBase.services.contracts.AddonsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * The class is responsible for managing the date on our homepage
 */

@Controller
@RequestMapping("/")
public class HomeController {

    private AddonsService addonsService;
    private Mapper mapper;

    @Autowired
    public HomeController(AddonsService addonsService, Mapper mapper) {
        this.addonsService = addonsService;
        this.mapper = mapper;
    }

    @GetMapping
    public String landingPage(Model model){
        model.addAttribute("featuredAddons", mapper.addonListToAddonCardList( addonsService.getFeaturedAddons()) );
        model.addAttribute("popularAddons",mapper.addonListToAddonCardList( addonsService.getPopularAddons()));
        model.addAttribute("newestAddons",mapper.addonListToAddonCardList( addonsService.getNewestAddons()));
        return "landing-page";
    }

    @GetMapping("/access-denied")
    public String showAccessDenied() {
        return "access-denied";
    }
}
