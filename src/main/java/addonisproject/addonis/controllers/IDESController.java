package addonisproject.addonis.controllers;

import addonisproject.addonis.addonsDataBase.services.contracts.IDEService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * The IDEs controller returns a list of the predefined IDEs for which our application supports addons.
 * The information is needed when filtering the Addons by IDE.
 */
@Controller
@RequestMapping("/ides")
public class IDESController {

    private IDEService ideService;

    @Autowired
    public IDESController(IDEService ideService) {
        this.ideService = ideService;
    }

    @GetMapping
    public String getAll(Model model){
        model.addAttribute("ides", ideService.getAll());
        return "ides";
    }
}
