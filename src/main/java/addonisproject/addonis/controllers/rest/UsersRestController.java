package addonisproject.addonis.controllers.rest;

import addonisproject.addonis.addonsDataBase.models.Mapper;
import addonisproject.addonis.addonsDataBase.models.addons.UserDetails;
import addonisproject.addonis.addonsDataBase.models.dtos.UserDTO;
import addonisproject.addonis.addonsDataBase.services.contracts.UsersService;
import addonisproject.addonis.exceptions.UserRegistrationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static addonisproject.addonis.utils.ValidatorHelper.isAdmin;

/**
 * The class delivers the Api functionality related to users.
 */
@RestController
@RequestMapping("api/users")
public class UsersRestController {

    private UserDetailsManager userDetailsManager;
    private UsersService usersService;
    private UserDetailsService userDetailsService;
    private Mapper mapper;

    @Autowired
    public UsersRestController(UserDetailsManager userDetailsManager,
                               UsersService usersService,
                               @Qualifier("userDetailsManager")
                                       UserDetailsService userDetailsService, Mapper mapper) {
        this.userDetailsManager = userDetailsManager;
        this.usersService = usersService;
        this.userDetailsService = userDetailsService;
        this.mapper = mapper;
    }

    @GetMapping("/{id}")
    public UserDetails getById(@PathVariable(name = "id") int id) {
        return usersService.getUserById(id);
    }

    @PostMapping("/register")
    public void registerUser(@RequestBody UserDTO userDTO ) {
        try {
            UserDetails userDetails = new UserDetails();
            userDetails = mapper.dtoToUser(userDTO, userDetails);
            usersService.createUser(userDetails, userDTO.getPassword(), userDTO.getRepeatPassword());
            throw new ResponseStatusException(HttpStatus.ACCEPTED);
        } catch (UserRegistrationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }


    @DeleteMapping("/{id}/delete")
    public UserDetails deleteUser(@PathVariable int id) {
        if (!isAdmin()) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        UserDetails userDetails = usersService.getUserById(id);
        org.springframework.security.core.userdetails.UserDetails springUserDetails;
        springUserDetails = userDetailsService.loadUserByUsername(userDetails.getUsername());
        springUserDetails = new User(
                springUserDetails.getUsername(),
                springUserDetails.getPassword(),
                false,
                springUserDetails.isAccountNonExpired(),
                springUserDetails.isCredentialsNonExpired(),
                springUserDetails.isAccountNonLocked(),
                springUserDetails.getAuthorities());
        userDetailsManager.updateUser(springUserDetails);
        usersService.deleteUser(userDetails);
        return userDetails;
    }
}
