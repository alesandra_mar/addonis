package addonisproject.addonis.addonsDataBase.repos;


import addonisproject.addonis.addonsDataBase.models.addons.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
/**
 * The UserDetailsRepository is used for managing entries to the user details table in our database. In addition
 * to the predefined by JPA methods, it provides functionality for getting user by name
 */
@Repository
public interface UserDetailsRepository extends JpaRepository<UserDetails, Integer> {

    @Query("select u from UserDetails u where u.username = ?1")
    Optional<UserDetails> getByName(String name);
}
