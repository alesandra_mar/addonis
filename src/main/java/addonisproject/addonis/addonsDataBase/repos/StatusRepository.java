package addonisproject.addonis.addonsDataBase.repos;

import addonisproject.addonis.addonsDataBase.models.addons.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The StatusRepository is used for managing entries to the status table in our database.
 */
@Repository
public interface StatusRepository extends JpaRepository<Status, Integer> {
}
