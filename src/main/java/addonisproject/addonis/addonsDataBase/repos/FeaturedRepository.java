package addonisproject.addonis.addonsDataBase.repos;

import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.addons.Featured;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import java.util.List;

/**
 * The FeaturedRepository is responsible for managing entries to the featured table.
 * Additional method for retrieving all addons in the featured table.
 */
@Repository
public interface FeaturedRepository extends JpaRepository<Featured,Integer> {

    @Query("select addon from Featured")
    List<Addon> getFeatured();

    @Query("select f from Featured f where f.addon.id = ?1")
    Featured getByAddonId(int id);
}
