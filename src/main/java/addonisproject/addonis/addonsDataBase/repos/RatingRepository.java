package addonisproject.addonis.addonsDataBase.repos;

import addonisproject.addonis.addonsDataBase.models.addons.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The RatingJPARepository is responsible for managing entries to the ratings table.
 * Additional methods for retrieving all ratings of an addon, checking if rating exists and
 * updating ratings.
 */
@Repository
public interface RatingRepository extends JpaRepository<Rating, Integer> {

    @Query("select r from Rating r  where r.addon.id =:id")
    List<Rating> getAddonRatings(int id);

    @Query("select case when (count(r) > 0) then true else false end from Rating r where r.addon.id =:addonId and r.userDetails.id = :userId")
    Boolean checkRatingExists(int addonId, int userId);

    @Transactional
    @Modifying
    @Query("UPDATE Rating r SET r.rating = :rating where r.addon.id =:addonId and r.userDetails.id = :userId")
    void updateRating(int addonId, int userId, int rating);
}
