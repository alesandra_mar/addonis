package addonisproject.addonis.addonsDataBase.repos;

import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The AddonsRepository is used for managing entries to the addons table in our database. In addition
 * to the predefined by JPA methods, it provides functionality for searching by name,
 * checking if name is already taken and updating the downloads counter.
 */
@Repository
public interface AddonsRepository extends JpaRepository<Addon, Integer> {

    @Query("select a from Addon a where a.name = ?1")
    Optional<Addon> getByName(String name);

    @Query("select a from Addon a where a.name like %?1%")
    List<Addon> searchByName(String name);

    @Query("select case when (count(a) > 0) then true else false end from Addon a where a.name = :name")
    Boolean checkAddonNameExists(String name);

    @Transactional
    @Modifying
    @Query("UPDATE Addon a SET a.numberDownloads = a.numberDownloads + 1 where a.id = :id")
    void updateNumberDownloadsOnClick(int id);
}
