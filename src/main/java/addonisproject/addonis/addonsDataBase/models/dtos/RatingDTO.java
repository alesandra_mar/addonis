package addonisproject.addonis.addonsDataBase.models.dtos;

/**
 * The RatingDTO class is used when users rate an addon.
 * It holds information about the rating number
 */
public class RatingDTO {

    private int rating;

    public RatingDTO() {
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
