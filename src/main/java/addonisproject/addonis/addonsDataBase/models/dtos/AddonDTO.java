package addonisproject.addonis.addonsDataBase.models.dtos;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * The AddonDTO is used for transferring data when creating or updating an addon.
 * An AddonDTO is received when an user registers a new addon.Also an AddonDTO is used
 * to visualise the existing information when users update addons.
 */
public class AddonDTO {

    @Size(min = 2, max = 30, message = "The name should be between 2 and 30 symbols")
    @NotBlank
    private String name;

    @NotBlank
    private String description;

    @NotBlank
    private String originLink;

    @NotBlank
    private String ide;

    private MultipartFile file;

    public AddonDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOriginLink() {
        return originLink;
    }

    public void setOriginLink(String originLink) {
        this.originLink = originLink;
    }

    public String getIde() {
        return ide;
    }

    public void setIde(String ide) {
        this.ide = ide;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
