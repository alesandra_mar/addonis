package addonisproject.addonis.addonsDataBase.models;

import java.util.Date;

/**
 * A model class that keeps the information about the last commit date and message of a repo
 * used in Api Service
 */
public class Commit {

    private Date lastCommitDate;
    private String message;

    public Commit(Date lastCommitDate, String message) {
        this.lastCommitDate = lastCommitDate;
        this.message = message;
    }

    public Date getLastCommitDate() {
        return lastCommitDate;
    }

    public void setLastCommitDate(Date lastCommitDate) {
        this.lastCommitDate = lastCommitDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
