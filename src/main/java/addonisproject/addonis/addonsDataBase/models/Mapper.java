package addonisproject.addonis.addonsDataBase.models;

import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.addons.IDE;
import addonisproject.addonis.addonsDataBase.models.addons.UserDetails;
import addonisproject.addonis.addonsDataBase.models.dtos.AddonCardDTO;
import addonisproject.addonis.addonsDataBase.models.dtos.AddonDTO;
import addonisproject.addonis.addonsDataBase.models.dtos.UserDTO;
import addonisproject.addonis.addonsDataBase.services.contracts.APIService;
import addonisproject.addonis.addonsDataBase.services.contracts.IDEService;
import addonisproject.addonis.addonsDataBase.services.contracts.RatingService;
import addonisproject.addonis.binaryContentDataBase.models.BinaryContent;
import addonisproject.addonis.binaryContentDataBase.services.contracts.BinaryContentService;
import addonisproject.addonis.exceptions.ApiConnectionException;
import addonisproject.addonis.exceptions.FileProblemException;
import addonisproject.addonis.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static addonisproject.addonis.utils.Constants.FILE_PROBLEM_ENCOUNTERED;

@Component
public class Mapper {

    private BinaryContentService binaryContentService;
    private IDEService ideService;
    private APIService apiService;
    private RatingService ratingService;

    @Autowired
    public Mapper(BinaryContentService binaryContentService, IDEService ideService,
                  APIService apiService, RatingService ratingService) {
        this.binaryContentService = binaryContentService;
        this.ideService = ideService;
        this.apiService = apiService;
        this.ratingService = ratingService;
    }

    public UserDetails dtoToUser(UserDTO userDTO, UserDetails userDetails) {
        userDetails.setUsername(userDTO.getUsername());
        userDetails.setEmail(userDTO.getEmail());
        userDetails.setFirstName(userDTO.getFirstName());
        userDetails.setLastName(userDTO.getLastName());
        userDetails.setEnabled(true);
        return userDetails;
    }

    public Addon dtoToAddon(AddonDTO addonDTO, Addon addon){
        addon.setName(addonDTO.getName());
        addon.setDescription(addonDTO.getDescription());
        IDE ide = ideService.getIdeByName(addonDTO.getIde());
        addon.setIde(ide);
        if (addonDTO.getFile() != null) {
            if (addonDTO.getFile().getSize() != 0) {
                try {
                    BinaryContent binaryContent = new BinaryContent();
                    binaryContent.setName(addonDTO.getFile().getOriginalFilename());
                    binaryContent.setType(addonDTO.getFile().getContentType());
                    binaryContent.setData(addonDTO.getFile().getBytes());
                    binaryContentService.createBinaryContent(binaryContent);
                    addon.setBinaryContent(binaryContent.getId());
                } catch (IOException e) {
                    throw new FileProblemException(String.format(FILE_PROBLEM_ENCOUNTERED, e.getMessage()));
                }
            }
        }
        addon.setOriginLink(addonDTO.getOriginLink());
        return addon;
    }

    public AddonDTO addonToDTO(AddonDTO addonDTO, Addon addon) {
        addonDTO.setName(addon.getName());
        addonDTO.setDescription(addon.getDescription());
        addonDTO.setIde(addon.getIde().getIDEName());
        addonDTO.setOriginLink(addon.getOriginLink());
        return addonDTO;
    }

    public AddonCardDTO addonToCard(Addon addon) {
        AddonCardDTO addonCardDTO = new AddonCardDTO();
        addonCardDTO.setId(addon.getId());
        addonCardDTO.setName(addon.getName());
        addonCardDTO.setIde(addon.getIde().getIDEName());
        addonCardDTO.setUploadDate(simplifyDateFormat(addon.getUploadDate()));
        addonCardDTO.setNumberOfDownloads(addon.getNumberDownloads());
        addonCardDTO.setStatus(addon.getStatus().getStatus());
        addonCardDTO.setRating(ratingService.getAvgRating(addon));
        try {
            Date lastCommitDate = apiService.getLastCommit(addon).getLastCommitDate();
            addonCardDTO.setLastCommitDate(simplifyDateFormat(lastCommitDate));
        } catch (ApiConnectionException e) {
            addonCardDTO.setLastCommitDate(e.getMessage());
        }
        return addonCardDTO;
    }

    public String simplifyDateFormat(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        return dateFormat.format(date);
    }

    public List<AddonCardDTO> addonListToAddonCardList(List<Addon> addonList) {
        return addonList.stream()
                .map(this::addonToCard)
                .collect(Collectors.toList());
    }
}
