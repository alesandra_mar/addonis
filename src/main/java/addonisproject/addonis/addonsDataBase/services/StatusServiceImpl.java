package addonisproject.addonis.addonsDataBase.services;

import addonisproject.addonis.addonsDataBase.models.addons.Status;
import addonisproject.addonis.addonsDataBase.repos.StatusRepository;
import addonisproject.addonis.addonsDataBase.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

import static addonisproject.addonis.utils.Constants.*;

/**
 * Implementation of StatusService interface, used for retrieving status by Id from the database
 */

@Service
public class StatusServiceImpl implements StatusService {

    private StatusRepository statusRepository;

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public Status getStatusById(int id) {
        return statusRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(ENTITY_NOT_FOUND_ERROR_MESSAGE, STATUS_TYPE)));
    }
}
