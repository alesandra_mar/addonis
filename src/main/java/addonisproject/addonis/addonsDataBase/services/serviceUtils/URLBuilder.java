package addonisproject.addonis.addonsDataBase.services.serviceUtils;

/**
 * A simple helper class with a single method, which serves to create an
 * endpoint from a Original Repo link and desired purpose.
 * The resulting String will be used by the API service class;
 */
public class URLBuilder {

    public static String createAPIUrl(String url, String type) {
        StringBuilder sb = createBaseURL(url,type);
        return sb.toString();
    }

    public static String createAPIUrl(String url, String type, int page, int per_page) {
        StringBuilder sb = createBaseURL(url,type);
        sb.append("?");
        sb.append("page=");
        sb.append(page);
        sb.append("&per_page=");
        sb.append(per_page);
        return sb.toString();
    }

    private static StringBuilder createBaseURL(String originalURL ,String type) {
        String[] url = originalURL.split("/");
        StringBuilder sb = new StringBuilder("https://api.github.com/repos/");
        for (int i = url.length - 2; i < url.length; i++) {
            sb.append(url[i]);
            sb.append("/");
        }
        sb.append(type);
        return sb;
    }
}
