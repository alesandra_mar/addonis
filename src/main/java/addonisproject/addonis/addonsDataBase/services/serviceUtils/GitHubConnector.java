package addonisproject.addonis.addonsDataBase.services.serviceUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

import static addonisproject.addonis.utils.Constants.*;

/**
 * The JsonReader class send request and receives the response from the GitHub API
 * in byte[] through the InputStream class. The byte[] is then converted to String.
 * BufferedRead and InputStreamReader classes are used for this purpose.
 * The resulting String is used to create an JsonArray[]
 */
public class GitHubConnector {

    public static JSONArray readJsonFromUrl(String url) throws IOException, JSONException {
        return new JSONArray((getConnection(url)));
    }

    public static JSONObject readJsonObjectFromUrl(String url) throws IOException, JSONException {
        return new JSONObject((getConnection(url)));
    }

    public static String getReadMeFromURL(String url) throws IOException, JSONException {
        try (BufferedInputStream inputStream = new BufferedInputStream(new URL(url).openStream())) {
            byte[] contents = new byte[32768];
            int bytesRead = 0;
            StringBuilder strFileContents = new StringBuilder();
            while ((bytesRead = inputStream.read(contents)) != -1) {
                strFileContents.append(new String(contents, 0, bytesRead));
            }
            return strFileContents.toString();
        }
    }

    private static String getConnection(String url) throws IOException {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        String userCredentials = USERNAME + ":" + PASSWORD;
        String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));
        con.setRequestMethod("GET");
        con.addRequestProperty("Authorization", basicAuth);
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(
                        con.getInputStream()));
        String result = readStream(rd);
        con.disconnect();
        return result;
    }

    private static String readStream(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
}
