package addonisproject.addonis.addonsDataBase.services.contracts;

import addonisproject.addonis.addonsDataBase.models.addons.Status;

public interface StatusService {

    Status getStatusById(int id);
}
