package addonisproject.addonis.addonsDataBase.services.contracts;

import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.addons.Tag;
import addonisproject.addonis.addonsDataBase.models.dtos.TagDTO;

public interface TagsService {

    Tag getTagByName(String name);

    void addTagToAddon(Addon addon, TagDTO tagDTO);

    void removeTagFromAddon(Addon addon, TagDTO tagDTO);
}

