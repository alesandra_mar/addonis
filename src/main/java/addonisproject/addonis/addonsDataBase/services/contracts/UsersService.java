package addonisproject.addonis.addonsDataBase.services.contracts;

import addonisproject.addonis.addonsDataBase.models.addons.UserDetails;

import java.util.List;

public interface UsersService {

    UserDetails getUserByName(String name);

    UserDetails getUserById(int id);

    List<UserDetails> getAllUsers();

    void createUser(UserDetails userDetails, String password,String repeatPassword);

    void deleteUser(UserDetails userDetails);

    List<UserDetails> getAllNonAdmins();
}
