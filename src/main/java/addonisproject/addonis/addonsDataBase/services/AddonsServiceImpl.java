package addonisproject.addonis.addonsDataBase.services;

import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.addons.Featured;
import addonisproject.addonis.addonsDataBase.models.addons.Status;
import addonisproject.addonis.addonsDataBase.models.dtos.AddonCardDTO;
import addonisproject.addonis.addonsDataBase.repos.AddonsRepository;
import addonisproject.addonis.addonsDataBase.repos.FeaturedRepository;
import addonisproject.addonis.addonsDataBase.repos.StatusRepository;
import addonisproject.addonis.addonsDataBase.services.contracts.AddonsService;
import addonisproject.addonis.exceptions.DuplicateEntityException;
import addonisproject.addonis.exceptions.SizeExceededException;
import addonisproject.addonis.utils.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

import static addonisproject.addonis.utils.Constants.*;
import static java.util.Optional.ofNullable;

/**
 * Implementation of AddonService interface, responsible for CRUD operations over addons, logic for
 * retrieving, manipulating addons from database by different criteria
 */

@Service
public class AddonsServiceImpl implements AddonsService {

    private AddonsRepository addonsRepository;
    private StatusRepository statusRepository;
    private FeaturedRepository featuredRepository;
    private Helper helper;

    @Autowired
    public AddonsServiceImpl(AddonsRepository addonsRepository,
                             StatusRepository statusRepository,
                             FeaturedRepository featuredRepository,
                             Helper helper) {
        this.addonsRepository = addonsRepository;
        this.statusRepository = statusRepository;
        this.featuredRepository = featuredRepository;
        this.helper = helper;
    }

    @Override
    public List<Addon> getAll() {
        return addonsRepository.findAll();
    }

    @Override
    public List<Addon> getAllStatusApproved() {
        return addonsRepository.findAll()
                .stream()
                .filter(addon -> addon.getStatus().getId() == 2)
                .collect(Collectors.toList());
    }

    @Override
    public List<Addon> getAllStatusPending() {
        return addonsRepository.findAll()
                .stream()
                .filter(addon -> addon.getStatus().getId() == 1)
                .collect(Collectors.toList());
    }

    @Override
    public Addon getById(int id) {
        return addonsRepository.findById(id).orElseThrow(()
                -> new EntityNotFoundException(String.format(ENTITY_NOT_FOUND_ERROR_MESSAGE, ADDON_TYPE)));
    }

    @Override
    public Addon getByName(String name) {
        return addonsRepository.getByName(name).orElseThrow(()
                -> new EntityNotFoundException(String.format(ENTITY_NOT_FOUND_ERROR_MESSAGE, ADDON_TYPE)));
    }

    @Override
    public List<Addon> searchByName(String name) {
        return addonsRepository.searchByName(name)
                .stream()
                .filter(addon -> addon.getStatus().getId() == 2)
                .collect(Collectors.toList());
    }

    @Override
    public void createAddon(Addon addon) {
        checkAddonNameExists(addon);
        addonsRepository.saveAndFlush(addon);
    }

    @Override
    public void updateAddon(Addon addon) {
        checkAddonNameDuplicate(addon.getId(), addon.getName());
        addonsRepository.save(addon);
    }

    @Override
    public void deleteAddon(Addon addon) {
        Optional<Status> status = statusRepository.findById(3);
        status.ifPresent(addon::setStatus);
        addonsRepository.save(addon);
    }

    @Override
    public void approveAddon(Addon addon) {
        Optional<Status> status = statusRepository.findById(2);
        status.ifPresent(addon::setStatus);
        addonsRepository.save(addon);
    }

    @Override
    public List<Addon> getFeaturedAddons() {
        List<Addon> addonList = featuredRepository.getFeatured()
                .stream()
                .filter(addon -> addon.getStatus().getId() == 2)
                .collect(Collectors.toList());
        return addonList;
    }

    @Override
    public void addToFeatured(Addon addon) {
        List<Addon> addonList = featuredRepository.getFeatured();
        if (addonList.size() == FEATURED_ADDONS_LIMIT) {
            throw new SizeExceededException(FEATURED_ADDONS_LIMIT, Addon.class.toString(), FEATURED_LIST);
        }
        checkAddonNameExists(addon, addonList);
        Featured featured = new Featured();
        featured.setAddon(addon);
        featuredRepository.save(featured);
    }

    @Override
    public void removeFromFeatured(Addon addon) {
        Featured featured = featuredRepository.getByAddonId(addon.getId());
        featuredRepository.delete(featured);
    }

    @Override
    public List<Addon> getPopularAddons() {
        List<Addon> addonList = getAllStatusApproved().stream()
                .sorted(Collections.reverseOrder(Comparator.comparing(Addon::getNumberDownloads)))
                .collect(Collectors.toList());
        return addonList.stream().limit(5).collect(Collectors.toList());
    }

    @Override
    public List<Addon> getNewestAddons() {
        List<Addon> addonList = getAllStatusApproved().stream()
                .sorted(Collections.reverseOrder(Comparator.comparing(Addon::getUploadDate)))
                .collect(Collectors.toList());
        return addonList.stream().limit(5).collect(Collectors.toList());
    }

    @Override
    public List<Addon> getUserApprovedAddons(int id) {
        List<Addon> addonList = getAllStatusApproved()
                .stream()
                .filter(addon -> addon.getCreator().getId() == id)
                .collect(Collectors.toList());
        return addonList;
    }

    @Override
    public List<Addon> getUserPendingAddons(int id) {
        List<Addon> addonList = getAllStatusPending()
                .stream()
                .filter(addon -> addon.getCreator().getId() == id)
                .collect(Collectors.toList());
        return addonList;
    }

    @Override
    public List<Addon> searchAddons(String ide, String name, String sortCategory) {
        Optional<String> optionalIde = ofNullable(ide);
        Optional<String> optionalName = ofNullable(name);
        Optional<String> optionalSortCategory = ofNullable(sortCategory);
        List<Addon> addonList;
        if (optionalName.isPresent()) {
            addonList = searchByName(name);
        } else {
            addonList = getAllStatusApproved();
        }
        if (optionalIde.isPresent()) {
            addonList = filterByIDE(ide, addonList);
        }
        if (optionalSortCategory.isPresent()) {
            addonList = sort(sortCategory, addonList);
        }
        return addonList;
    }

    @Override
    public List<AddonCardDTO> paginate(List<AddonCardDTO> addonList, int page) {
        int beginningIndex = ((page - 1) * PER_PAGE_SEARCH);
        int endIndex = (page * PER_PAGE_SEARCH);
        int listSize = addonList.size();
        if (listSize > beginningIndex) {
            if (listSize > endIndex) {
                return addonList.subList(beginningIndex, endIndex);
            } else return addonList.subList(beginningIndex, listSize);
        }
        return new ArrayList<>();
    }

    @Override
    public void updateNumberDownloadsOnClick(int id) {
        Addon addon = addonsRepository.getOne(id);
        addonsRepository.updateNumberDownloadsOnClick(id);
        addonsRepository.save(addon);
    }

    private void checkAddonNameExists(Addon addon) {
        if (addonsRepository.checkAddonNameExists(addon.getName())) {
            throw new DuplicateEntityException(ADDON_TYPE, addon.getName());
        }
    }

    private void checkAddonNameDuplicate(int id, String name) {
        if (!getById(id).getName().equalsIgnoreCase(name) &&
                addonsRepository.checkAddonNameExists(name)) {
            throw new DuplicateEntityException(ADDON_TYPE, name);
        }
    }

    private void checkAddonNameExists(Addon addon, List<Addon> addonList) {
        if (addonList
                .stream()
                .map(Addon::getName)
                .anyMatch(addon1 -> addon1.equals(addon.getName()))) {
            throw new DuplicateEntityException(ADDON_TYPE, addon.getName());
        }
    }

    private List<Addon> filterByIDE(String ide, List<Addon> addonList) {
        return addonList.stream()
                .filter(addon -> addon.getIde().getIDEName().equals(ide))
                .collect(Collectors.toList());
    }

    private List<Addon> sort(String sortCategory, List<Addon> addonList) {
        switch (sortCategory) {
            case "name":
                return helper.sortByName(addonList);
            case "downloads":
                return helper.sortByDownloads(addonList);
            case "uploadDate":
                return helper.sortByUploadDate(addonList);
            case "commitDate":
                return helper.sortByLastCommitDate(addonList);
            default:
                return addonList;
        }
    }
}
