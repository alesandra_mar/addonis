package addonisproject.addonis.addonsDataBase.services;

import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.addons.Rating;
import addonisproject.addonis.addonsDataBase.repos.RatingRepository;
import addonisproject.addonis.addonsDataBase.services.contracts.RatingService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Implementation of RatingService interface, responsible for calculating the average rating for each addon
 * and the logic for rate addons
 */

@Service
public class RatingServiceImpl implements RatingService {

    private RatingRepository ratingRepository;

    public RatingServiceImpl(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    @Override
    public void rateAddon(Rating rating) {
        int addonId = rating.getAddon().getId();
        int userId = rating.getUserDetails().getId();
        if (ratingRepository.checkRatingExists(addonId, userId)) {
            ratingRepository.updateRating(addonId, userId, rating.getRating());
        } else {
            ratingRepository.save(rating);
        }
    }

    @Override
    public double getAvgRating(Addon addon) {
        List<Rating> ratings = ratingRepository.getAddonRatings(addon.getId());
        if (ratings.size() != 0) {
            return ratings.stream().map(Rating::getRating).mapToInt(value -> value).average().getAsDouble();
        }
        return 0;
    }
}
