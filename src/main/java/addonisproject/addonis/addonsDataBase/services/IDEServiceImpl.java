package addonisproject.addonis.addonsDataBase.services;

import addonisproject.addonis.addonsDataBase.models.addons.IDE;
import addonisproject.addonis.addonsDataBase.repos.IDERepository;
import addonisproject.addonis.addonsDataBase.services.contracts.IDEService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

import java.util.List;

import static addonisproject.addonis.utils.Constants.ENTITY_NOT_FOUND_ERROR_MESSAGE;
import static addonisproject.addonis.utils.Constants.IDE_TYPE;

/**
 * Implementation of IDEService interface, responsible for retrieving Ides from database by different criteria
 */
@Service
public class IDEServiceImpl implements IDEService {

    private IDERepository ideRepository;

    @Autowired
    public IDEServiceImpl(IDERepository ideRepository) {
        this.ideRepository = ideRepository;
    }

    @Override
    public List<IDE> getAll() {
        return ideRepository.findAll();
    }

    @Override
    public IDE getIdeByName(String name) {
        return ideRepository.getByName(name).
                orElseThrow(() -> new EntityNotFoundException(String.format(ENTITY_NOT_FOUND_ERROR_MESSAGE, IDE_TYPE)));
    }
}
