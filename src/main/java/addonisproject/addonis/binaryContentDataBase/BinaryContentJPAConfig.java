package addonisproject.addonis.binaryContentDataBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(basePackages = "addonisproject.addonis.binaryContentDataBase.repos",
        entityManagerFactoryRef = "binaryEntityManagerFactory",
        transactionManagerRef = "binaryTransactionManager")
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
public class BinaryContentJPAConfig {

    private String bcDbUrl, bcDbUsername, bcDbPass;

    @Autowired
    BinaryContentJPAConfig(Environment env){
        bcDbUrl = env.getProperty("binarycontent.database.url");
        bcDbUsername = env.getProperty("addons.database.username");
        bcDbPass = env.getProperty("addons.database.password");
    }

    @Bean(name = "secondaryDataBase")
    public DataSource binaryDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl(bcDbUrl);
        dataSource.setUsername(bcDbUsername);
        dataSource.setPassword(bcDbPass);
        return dataSource;
    }

    @Bean
    public EntityManagerFactory binaryEntityManagerFactory() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("addonisproject.addonis.binaryContentDataBase.models");
        factory.setDataSource(binaryDataSource());
        factory.afterPropertiesSet();
        return factory.getObject();
    }

    @Bean
    public PlatformTransactionManager binaryTransactionManager() {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(binaryEntityManagerFactory());
        return txManager;
    }
}

