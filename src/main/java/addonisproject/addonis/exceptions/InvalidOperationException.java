package addonisproject.addonis.exceptions;

import addonisproject.addonis.utils.Constants;

/**
 * Custom exception thrown when an user attempts to perform an action without proper authorization
 */
public class InvalidOperationException extends RuntimeException {

    public InvalidOperationException(String message) {
        super(message);
    }
}