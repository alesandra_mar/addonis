package addonisproject.addonis.exceptions;

/**
 * Custom exception thrown when attempt to add an entry to the database is made, when entry with
 * the same characteristics, defined as unique, already exists.
 */
public class DuplicateEntityException extends RuntimeException {

    public DuplicateEntityException(String message){
        super(message);
    }

    public DuplicateEntityException(String type, String name) {
        super(String.format("%s %s already exists", type, name));
    }
}

