package addonisproject.addonis.exceptions;

/**
 * Custom exception throw when conditions for creating new users are not meet.
 */
public class UserRegistrationException extends RuntimeException{

    public UserRegistrationException(String message) {
        super(message);
    }
}
