package addonisproject.addonis.exceptions;

/**
 * Custom exception thrown when attempts to connect to GitHub Api fail
 */
public class ApiConnectionException extends RuntimeException {

    public ApiConnectionException(String message){
        super(message);
    }

    public ApiConnectionException(String type, String name) {
        super(String.format("%s %s already exists", type, name));
    }
}
