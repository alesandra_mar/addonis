package addonisproject.addonis.utils;

import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.addons.UserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.security.Principal;

/**
 * A simple helper class with methods, that will be used when needed in different classes
 */
public class ValidatorHelper {

    public static boolean isAdmin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getAuthorities().stream().anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));
    }

    public static boolean isCreator(Addon addon, UserDetails user) {
        return addon.getCreator().getUsername().equals(user.getUsername());
    }

    public static boolean isCreator(Addon addon, Principal principal) {
        return addon.getCreator().getUsername().equals(principal.getName());
    }
}
