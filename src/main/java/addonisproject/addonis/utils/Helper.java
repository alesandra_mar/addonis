package addonisproject.addonis.utils;

import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.services.contracts.APIService;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class Helper {

    private APIService apiService;

    public Helper(APIService apiService) {
        this.apiService = apiService;
    }

    public List<Addon> sortByLastCommitDate(List<Addon> addonList) {
        return addonList
                .stream()
                .sorted(Collections.reverseOrder(
                        Comparator.comparing(addon ->
                                apiService.getLastCommit(addon).getLastCommitDate())))
                .collect(Collectors.toList());
    }

    public List<Addon> sortByUploadDate(List<Addon> addonList) {
        return addonList
                .stream()
                .sorted(Collections.reverseOrder(
                        Comparator.comparing(Addon::getUploadDate)))
                .collect(Collectors.toList());
    }

    public List<Addon> sortByDownloads(List<Addon> addonList) {
        return addonList
                .stream()
                .sorted(Collections.reverseOrder(
                        Comparator.comparing(Addon::getNumberDownloads)))
                .collect(Collectors.toList());
    }

    public List<Addon> sortByName(List<Addon> addonList) {
        return addonList
                .stream()
                .sorted(Comparator.comparing(Addon::getName))
                .collect(Collectors.toList());
    }
}
