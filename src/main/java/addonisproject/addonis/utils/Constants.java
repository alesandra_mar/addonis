package addonisproject.addonis.utils;

/**
 * A simple helper class for constants that replace the magic strings used in classes
 */
public class Constants {

    public final static String ENTITY_NOT_FOUND_ERROR_MESSAGE = "%s not found";
    public static final String USER_ID = "User ID";
    public final static String ADDON_TYPE = "Addon";
    public final static String USER_TYPE = "User";
    public final static String STATUS_TYPE = "Status";
    public final static String IDE_TYPE = "IDE";
    public final static String TAG_TYPE = "Tag";
    public final static String BINARY_TYPE = "Binary";
    public final static String RESOURCE_TYPE = "Resource";
    public static final String COMMITS = "commits";
    public static final String PULLS = "pulls";
    public static final String ISSUES = "issues";
    public static final String README="readme";
    public static final String USERNAME ="nikipet";
    public static final String PASSWORD ="matematika_23";
    public static final String FEATURED_LIST = "featured list";

    public static final int PER_PAGE = 100;
    public static final int BEGINNING_PAGE = 1;
    public static final int FEATURED_ADDONS_LIMIT = 10;
    public static final int PER_PAGE_SEARCH = 10;

    public static final String USERNAME_ALREADY_EXISTS_ERROR_MESSAGE = "User with the same username already exists!";
    public static final String THIS_EMAIL_ALREADY_REGISTERED_ERROR_MESSAGE = "This email is already registered!";
    public static final String PASSWORDS_MISMATCH_ERROR_MESSAGE = "The passwords do not match";
    public static final String CONNECTION_ERROR_MESSAGE = "Could not connect to GitHub servers";
    public static final String NO_ADDONS_MATCH_CRITERIA_ERROR_MESSAGE = "Addons matching the criteria ";
    public static final String ADMIN_AUTHORIZATION_REQUIRED = "Admin Authorization required";
    public static final String USER_NOT_UNAUTHORIZED_TO_UPDATE_ADDON = "Only admins and creator can update addons!";
    public static final String USER_NOT_UNAUTHORIZED_TO_ADD_TAGS = "Only admins and creator can add tags to addons!";
    public static final String USERNAME_LENGTH_ERROR_MESSAGE = "Username must be between 6 and 20 characters long";
    public static final String PASSWORD_LENGTH_ERROR_MESSAGE = "Password length must be at least 8 characters long";
    public static final String LOG_FOLDER = "src\\main\\java\\addonisproject\\addonis\\logs\\";

    public static final String FILE_PROBLEM_ENCOUNTERED = "Problem encountered with the uploaded file: %s";
}
