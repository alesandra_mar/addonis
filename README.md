# Addonis

#### Trello board https://trello.com/b/EkhESaEn/addonis

> Our team developed a web application called **Addonis** that gives users the opportunity 
to manage addons. Each addon has name, description, list of tags, rating, download link and 
origin github link. Some of the information about each addon is retrieved from the origin repository using GitHub API and then
visualize on the addon's page. Users can make their extensions available for everyone. The app is giving
them the opportunity to publish their own addons. Once the addon created, their status is still pending. 
the admins will then approve them. The extension is visible to the public only if it is approved. Furthermore, users
can rate addons, update and delete their own addons. Users have a profile page where they can find
their personal information like first, last name, email and username, see their pending and 
approved addons. Admins have full control over all addons and all users.

- **Without authentication** you can see : 

  - the landing page divided in 3 categories - **featured**(addons selected by admins), **popular**(top 5 addons with the most 
  downloads), **new**(top 5 newest addons)
  - the list with all the addons, you can search by name, filter them by IDE, sort them by name, number of downloads,
  upload date, last commit date.

  - the product page with all the information about each addon + download the plugin itself
  - the login page
  - the user registration form


- **Users** can : 

  - create new addons
  
  - update and delete their own addons
  - rate addons
  - see their profile page

- **Admins** can : 

  - create, modify, delete addons
  - approve addons 
  - see all the users and disable them

### Technologies

- *JDK 8*

- *IntelliJ IDEA* - as an IDE during the whole development process

- *Spring MVC* - as an application framework

- *Thymeleaf* - as a template engine in order to create an MVC application and to build dynamic web pages

- *MySQL* - as database

- *JPA* - storing, accessing, and managing objects in the database 

- *Spring Security* - for managing users, roles and authentication

- *Git* - for source control management and documentation / manual of the application

### Routes

URLs | Description
---------|---------
 */* | Landing page with three categories - featured, popular, new addons
 */search* | Page where all the addons are shown, their sort options
 */addons/pending* | Page where all the pending addons are shown (visible only for admins)
 */addons/{id}* | Addon product page with all the information
 */addon/{id}/update* | Form for update the addon
 */addons/new* | Form for create a new addon
 */ides* | Page with popular IDEs, possibility to filter addons by IDE
 */login* | Login User Page
 */register* | Register Form Page
 */users/{id}* | Profile page for every user
 */users/{id}/pending* | All the pending addons created by user

Database Diagram Screenshot
---

![database](/src/main/resources/static/screens/database-screenshot.png)

Website Screenshots
---

- Landing Page 
![homepage](/src/main/resources/static/screens/landing-page.png)

- All Addons Page
![addons](/src/main/resources/static/screens/all-addons.png)

- All Pending Addons Page
![pending-addons](/src/main/resources/static/screens/all-pending-addons.png)

- Addon Product Page
![product-page](/src/main/resources/static/screens/addon-page.png)

- Create new Addon form
![create-new-addon](/src/main/resources/static/screens/create-addon.png)

- Update Addon form
![update](/src/main/resources/static/screens/update-addon.png)

- All IDEs Page
![addons](/src/main/resources/static/screens/ides.png)

- Login Form
![login](/src/main/resources/static/screens/sign-in.png)

- Register Form
![register](/src/main/resources/static/screens/register.png)

- Profile Page
![profile](/src/main/resources/static/screens/user-page.png)

- About Page
![about](/src/main/resources/static/screens/about-page.png)

